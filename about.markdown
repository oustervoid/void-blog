---
title: About
permalink: /about/
---

personal blog for void-which-binds
{% highlight json %}
purpose: "space to explore healing trauma via interactive media, short stories about our trans & plural experience, code snippets to confuse & amuse"
{% endhighlight %}
