---
title:  "we found refuge in flamerock"
date:   2021-11-12 09:25:56 +0000
categories: game musings
tags: trauma healing borderlands tiny-tina
---
2021, morning of november 11th

we sit with hands hovering over a call notification. this is expected, we have plans to play a game with a friend. but despite having known the caller on the other end since the body was a child, there is a reluctance to answer.

we havent told them that we are plural. we feel the pain of inauthenticity set in as we place a mask over our language and expression. one day, we think briefly as the call connects, we'll find some way to tell them. 

we just need to find the right time and place.

2012, fall is ending

we sit next to our friend as the opening character select timer ticks down. weve waited for the sequel to borderlands for years and now we have less then a minute to determine who to play and what we will look like. 

the timer feels like it doesn't disappear as we enter into the first level. we power through wave after wave of enemy, a blur of motion from one plot point to the next. we laugh and curse with our friend and get lost in a place that mirrored the absurd and unflinching circumstances of our lives at this time.

we want to make these moments last forever. we reach the credits in less then a week of playing.

2021,  

we dont remember if we ever played this dlc when it came out. our friend doesnt remember either. the character creation timer is counting down. did we ever try this character? when was this released again? a handful of seconds remain. 

the timer ends. the story starts and we settle in for the expected frenzy of gunfights and gags that our memory attempts to recall.

2013,

the tiny rina dlc launches for borderlands 2. we never have a chance to play it.

2021,

we stand before a gate to the next level. and yet, neither us or our friend feel compelled to move forward. our conversation has shifted from the fond nostalgia of familiar characters and game mechanics to more difficult memories of our lives from roughly ten years ago.

my friend confides their recent mental health struggles and burnout, their voice shaking as they say that theyve always struggled in silence until now.

we watch as our friend sprints through the town of flamerock, a location introduced early in the dlc. we follow along, hopping across building rooftops and ropes across the crags that are scattered through the landscape.

we share our own realizations of the past about our neurodivergence and the neglect that lead to our unsustainable masking. we each laugh and choke up as the trauma and pain that had alway been within us finally finds language and expression after all these years.

we fire guns into the sky, leap off the map in dramatic fashion, our friend punches an npc so hard they explode, and all while laughing and crying so much that those emotions blur together.

they tell us "im sorry, that makes so many things make sense but im so sorry you went through that. you always seemed so mature, even before middle school it felt like you had already grown up and become someone. I feel like im still trying to figure out who I am and how to deal with all of this."

we hesitate, unsure of what to share, knowing that our plurality could finally be shared, possibly even understood with compassion. the words that flowed so freely just moments before now falter and give way to silence.

our friend interrupts the silence to say they think they found an easter egg. we join them at the edge of town.

doesnt that look like plank from ed edd and eddy? 

huh, yeah kinda. 

neat.

2013,

we dont have the words or the knowledge to express our experience, but we have a desperate need to be understood. there are a few people we could trust, but the words never seem to come when needed or the timing just never seems right.

we just need to find the right time and place.
