var store = [{
        "title": "zero index",
        "excerpt":"this post is mainly to debug issues with deploying.   find more about the project here: [gitlab-link]   ","categories": ["jekyll","update"],
        "tags": [],
        "url": "/jekyll/update/2021/11/12/welcome-to-jekyll.html",
        "teaser": null
      },{
        "title": "we found refuge in flamerock",
        "excerpt":"2021, morning of november 11th we sit with hands hovering over a call notification. this is expected, we have plans to play a game with a friend. but despite having known the caller on the other end since the body was a child, there is a reluctance to answer. we...","categories": ["game","musings"],
        "tags": ["trauma","healing","borderlands","tiny-tina"],
        "url": "/game/musings/2021/11/12/we-found-refuge-in-flamerock.html",
        "teaser": null
      }]
